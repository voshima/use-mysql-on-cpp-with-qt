# SIMPLE C++ QT app with MySQL 
This app for quick send emails with identical theme and body

## Getting Started
You can add your own inputs and change that are already there
folder src/ is unnecessary

## Liberaries in aplication
1. SqlDatabase		- to connect your MySql DB
2. Sql			- to debug Sql errors
3. SqlQuery		- to use MySql commands
4. SqlTableModel	- to load data from DB
5. TableView		- to show data from DB
6. MessageBox		- if there are errors

## Built With
- [QT](https://github.com/qt) - for UI and MySQL
- [SmtpClient-for-Qt](https://github.com/bluetiger9/SmtpClient-for-Qt) - for sending Emails
