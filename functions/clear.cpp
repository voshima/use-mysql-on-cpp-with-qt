#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::clearInputs() {
    ui->name->clear();
    ui->customer->clear();
    ui->email->clear();
    ui->link->clear();
}

// deleting pointers in pointers
void FUNCTIONS::deleteAll(QString *&name_of_product, QString *&customer, QString *&email, QString *&link) {
    delete name_of_product;
    delete customer;
    delete email;
    delete link;
}
